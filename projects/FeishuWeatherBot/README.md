# FeishuWeatherBot

借助 [Chatopera 飞书应用](https://chatopera.feishu.cn/docs/doccnnLcv5AuenV1HHSvgVWbJmd)，该示例程序实现一个能回答天气情况的飞书机器人。

更多使用介绍，参考 [https://chatopera.blog.csdn.net/](https://chatopera.blog.csdn.net/)。


![](../../assets/image-2021-05-14-095741.png)